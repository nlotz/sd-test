# Unproblematic code
print("Hello world")

# SAST should find a problem with this code
password = "mypassword"

try:
    do_something()
except:
    pass

# Secret Detection should find a problem with this code
SSN = '123-45-6789'

